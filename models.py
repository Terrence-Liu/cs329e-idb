# from flask import Flask
# from flask_sqlalchemy import SQLAlchemy
# import os
# import itertools
# import random

# db = SQLAlchemy()

# class Song(db.Model):
#     __tablename__ = 'songs'
	
#     song_name = db.Column(db.String(100))
#     album = db.Column(db.String(100))
#     band = db.Column(db.String(100))
#     year = db.Column(db.Integer)
#     genre = db.Column(db.String(50))
#     language = db.Column(db.String(10))
#     video = db.Column(db.String(1000), primary_key = True)
#     lyrics = db.Column(db.String(1000))

# class Album(db.Model):
#     __tablename__ = 'albums'
	
#     album_name = db.Column(db.String(100), primary_key = True)
#     band = db.Column(db.String(100))
#     year_released = db.Column(db.Integer)
#     highest_charting_song = db.Column(db.String(100))
#     genre = db.Column(db.String(50))
#     number_of_songs = db.Column(db.Integer)
#     image = db.Column(db.String(1000))

# class Band(db.Model):
#     __tablename__ = 'bands'
	
#     band_name = db.Column(db.String(100))
#     place_of_origin = db.Column(db.String(100))
#     year_started = db.Column(db.Integer)
#     genre = db.Column(db.String(50))
#     website = db.Column(db.String(1000), primary_key = True)
#     albums = db.Column(db.ARRAY(db.String(100)))
#     notable_songs = db.Column(db.ARRAY(db.String(100)))
#     members = db.Column(db.ARRAY(db.String(1000)))
#     image = db.Column(db.String(1000))


# db.drop_all()
# db.create_all()
# # End of models.py