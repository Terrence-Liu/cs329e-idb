import os
import sys
import unittest
from main import db, Song, Album, Band


class DBTestCases(unittest.TestCase):
    # testing insertion of songs
    def test_song_insert_1(self):
        db.session.query(Song).filter_by(video="video_link_1").delete()
        s1 = Song(song_name="Open Water", album="Flying Microtonal Banana", band="KGATLW",
                  year=2017, genre="Rock", language="English",
                  video="video_link_1", lyrics="lyrics_link_1")

        db.session.add(s1)
        db.session.commit()
        r = db.session.query(Song).filter_by(band="KGATLW").one()
        self.assertEqual(str(r.genre), 'Rock')
        db.session.query(Song).filter_by(band="KGATLW").delete()
        db.session.commit()

    def test_song_insert_2(self):
        db.session.query(Song).filter_by(video="video_link_1").delete()
        s1 = Song(song_name="Kids", album="Oracular Spectacular", band="MGMT",
                  year=2008, genre="Rock", language="English",
                  video="video_link_1", lyrics="lyrics_link_1")

        db.session.add(s1)
        db.session.commit()
        r = db.session.query(Song).filter_by(band="MGMT").one()
        self.assertEqual(str(r.song_name), 'Kids')
        db.session.query(Song).filter_by(band="MGMT").delete()
        db.session.commit()

    def test_song_insert_3(self):
        db.session.query(Song).filter_by(video="video_link_1").delete()
        s1 = Song(song_name="Spiderhead", album="Melophobia", band="Cage the Elephant",
                  year=2013, genre="Rock", language="English",
                  video="video_link_1", lyrics="lyrics_link_1")

        db.session.add(s1)
        db.session.commit()
        r = db.session.query(Song).filter_by(album="Melophobia").one()
        self.assertEqual(str(r.year), str(2013))
        db.session.query(Song).filter_by(album="Melophobia").delete()
        db.session.commit()

    # testing insertion of bands
    def test_band_insert_1(self):
        db.session.query(Band).filter_by(website="website_link_1").delete()
        b1 = Band(band_name="KGATLW", place_of_origin="Melbourne", year_started=2010, genre="Rock",
                  website="website_link_1", albums=["album_1", "album_2"], notable_songs=["notable_songs_1", "notable_songs_2"],
                  members=["member_1", "member_2"], image="image_link_1")

        db.session.add(b1)
        db.session.commit()
        r = db.session.query(Band).filter_by(place_of_origin="Melbourne").one()
        self.assertEqual(str(r.year_started), str(2010))
        db.session.query(Band).filter_by(place_of_origin="Melbourne").delete()
        db.session.commit()

    def test_band_insert_2(self):
        db.session.query(Band).filter_by(website="website_link_1").delete()
        b1 = Band(band_name="MGMT", place_of_origin="Conneticut, USA", year_started=2007, genre="Rock",
                  website="website_link_1", albums=["album_1", "album_2"], notable_songs=["notable_songs_1", "notable_songs_2"],
                  members=["member_1", "member_2"], image="image_link_1")

        db.session.add(b1)
        db.session.commit()
        r = db.session.query(Band).filter_by(image="image_link_1").one()
        self.assertEqual(len(r.notable_songs), 2)
        db.session.query(Band).filter_by(image="image_link_1").delete()
        db.session.commit()

    def test_band_insert_3(self):
        db.session.query(Band).filter_by(website="website_link_1").delete()
        b1 = Band(band_name="Oasis", place_of_origin="Manchester, England", year_started=1993, genre="Rock",
                  website="website_link_1", albums=["album_1", "album_2"], notable_songs=["notable_songs_1", "notable_songs_2", "notable_songs_3"],
                  members=["member_1", "member_2"], image="image_link_1")

        db.session.add(b1)
        db.session.commit()
        r = db.session.query(Band).filter_by(image="image_link_1").one()
        self.assertEqual(r.band_name, "Oasis")
        db.session.query(Band).filter_by(image="image_link_1").delete()
        db.session.commit()

    # testing insertion of album
    def test_album_insert_1(self):
        db.session.query(Album).filter_by(image="image_link_1").delete()
        b1 = Album(album_name="25", band="a-ha", year_released=1970,
                   highest_charting_song="Take On Me", genre="Pop", number_of_songs=79, image="image_link_1")

        db.session.add(b1)
        db.session.commit()
        r = db.session.query(Album).filter_by(image="image_link_1").one()
        self.assertEqual(r.band, "a-ha")
        db.session.query(Album).filter_by(image="image_link_1").delete()
        db.session.commit()

    def test_album_insert_2(self):
        db.session.query(Album).filter_by(image="image_link_1").delete()
        b1 = Album(album_name="Astro Lounge", band="Smash Mouth", year_released=1999,
                   highest_charting_song="All Star", genre="Power Pop", number_of_songs=78, image="image_link_1")

        db.session.add(b1)
        db.session.commit()
        r = db.session.query(Album).filter_by(image="image_link_1").one()
        self.assertEqual(r.genre, "Power Pop")
        db.session.query(Album).filter_by(image="image_link_1").delete()
        db.session.commit()

    def test_album_insert_3(self):
        db.session.query(Album).filter_by(image="image_link_1").delete()
        b1 = Album(album_name="Whenever You Need Somebody", band="Richard Astley", year_released=1987,
                   highest_charting_song="Never Gonna Give You Up", genre="Dance-pop", number_of_songs=1000, image="image_link_1")

        db.session.add(b1)
        db.session.commit()
        r = db.session.query(Album).filter_by(
            album_name="Whenever You Need Somebody").one()
        self.assertEqual(r.band, "Richard Astley")
        db.session.query(Album).filter_by(
            album_name="Whenever You Need Somebody").delete()
        db.session.commit()


if __name__ == '__main__':
    unittest.main()
