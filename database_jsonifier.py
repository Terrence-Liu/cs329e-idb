from openpyxl import load_workbook
import json
import sys

# takes in an integer x and spits out what
# excel would label the xth column
def convertNumToExcelColumnForm(x):
    possibilities = [chr(i) for i in range(65, 91)]
    answer = ""
    while(True):
        answer = answer+possibilities[(x % 26-1) % 26]
        x = (x-1)//26
        if(x <= 0):
            break
    return answer[::-1]


def getBands(f_name):
    wb = load_workbook(filename=f_name)
    bands_sheet = wb[wb.sheetnames[0]]
    num_columns = bands_sheet.max_column

    col_ranges = [convertNumToExcelColumnForm(
        i) for i in range(1, num_columns+1)]
    keys = [bands_sheet[i+'1'].value for i in col_ranges]
    num_bands = bands_sheet.max_row-1
    row_ranges = [str(i) for i in range(2, num_bands+2)]
    bands = []
    for i in range(len(row_ranges)):
        dict1 = {}
        for j in range(len(col_ranges)):
            dict1[keys[j]] = bands_sheet[col_ranges[j]+row_ranges[i]].value
        dict2 = {}
        albums = [str(dict1["Album "+str(j)])
                  for j in range(1, 11) if dict1["Album "+str(j)] != None]
        # needs to be changed if a band has albums or members > 10
        members = [str(dict1["Member "+str(j)])
                   for j in range(1, 11) if dict1["Member "+str(j)] != None]
        notable_songs = [str(dict1["Notable Song "+str(j)])
                         for j in range(1, 11) if dict1["Notable Song "+str(j)] != None]
        dict2 = {"Band Name": str(dict1["Band Name"]),
                 "Place of Origin": str(dict1["Place of Origin"]),
                 "Year Started": str(int(dict1["Year Started"])),
                 "Genre": str(dict1["Genre"]),
                 "Website": str(dict1["Website"]),
                 "Albums": albums, "Members": members,
                 "Notable Songs": notable_songs,
                 "Image": str(dict1["Image"])}
        bands.append(dict2)

    wb.close()
    return bands


def getAlbums(f_name):
    wb = load_workbook(filename=f_name)
    albums_sheet = wb[wb.sheetnames[0]]
    num_columns = albums_sheet.max_column

    col_ranges = [convertNumToExcelColumnForm(
        i) for i in range(1, num_columns+1)]
    keys = [albums_sheet[i+'1'].value for i in col_ranges]
    num_albums = albums_sheet.max_row-1
    row_ranges = [str(i) for i in range(2, num_albums+2)]
    albums = []
    for i in range(len(row_ranges)):
        dict1 = {}
        for j in range(len(col_ranges)):
            if(keys[j]=="Year Released"):
                dict1[keys[j]] = str(int(
                albums_sheet[col_ranges[j]+row_ranges[i]].value))
            elif(keys[j]=="Number of Songs"):
                dict1[keys[j]] = int(
                albums_sheet[col_ranges[j]+row_ranges[i]].value)
            else:
                dict1[keys[j]] = str(
                albums_sheet[col_ranges[j]+row_ranges[i]].value)
        albums.append(dict1)

    wb.close()
    return albums


def getSongs(f_name):
    wb = load_workbook(filename=f_name)
    songs_sheet = wb[wb.sheetnames[0]]
    num_columns = songs_sheet.max_column

    col_ranges = [convertNumToExcelColumnForm(
        i) for i in range(1, num_columns+1)]
    keys = [songs_sheet[i+'1'].value for i in col_ranges]
    num_songs = songs_sheet.max_row-1
    row_ranges = [str(i) for i in range(2, num_songs+2)]
    songs = []
    for i in range(len(row_ranges)):
        dict1 = {}
        for j in range(len(col_ranges)):
            if(keys[j]=="Year"):
              dict1[keys[j]] = str(int(
                songs_sheet[col_ranges[j]+row_ranges[i]].value))
            else:
              dict1[keys[j]] = str(
                songs_sheet[col_ranges[j]+row_ranges[i]].value)
            if(keys[j]=="Video"):
              dict1[keys[j]]=dict1[keys[j]].replace("watch?v=","embed/")
        songs.append(dict1)

    wb.close()
    return songs


# takes in data,jsonizes and sends to file f_name
def sendIntoFile(f_name, data):
    fout = open(f_name, 'w')
    json.dump(data, fout)
    fout.close()
    print("Made", f_name)


def main():
    # f_name="test_database.xlsx"
    sendIntoFile("bands.json", getBands("Bands.xlsx"))
    sendIntoFile("albums.json", getAlbums("Albums.xlsx"))
    sendIntoFile("songs.json", getSongs("songs.xlsx"))
    getSongs("Songs.xlsx")


if(__name__ == "__main__"):
    main()
